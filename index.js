//Create an expressjs API designated to port 4000
const express = require('express');

// a package that allows the creation of Schemas to model the data structure
// has access to a number of methods for manipulating our database
const mongoose = require('mongoose'); 

const app = express();

const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@course-booking.jmriv.mongodb.net/B157_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

//Set notification for connection success or failure
// Connection to the database 
let db = mongoose.connection;

// Allows to handle errors when initial connection is established
// If there's an error, will output in the console
db.on("error", console.error.bind(console, 'Connection Error'));

// If the connection is successful, will output in the console
db.once('open', () => console.log('Database connected'));


// Schemas determine the structure of the documents to be written in the database
// Schemas acts a blueprints to our data
// Creating a Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
});


// Model uses schemas and are used to create/instantiate objects that correspond to the schema
// Model must be in a singular form and capitalized
// The first parameter of the mongoose model method indicates the collection in where will be stored in the MongoDB Collection
// The second parameter is used to specify the schema of the documents
// Creating a model
const Task = mongoose.model('Task', taskSchema);


// Allows the app to read json data
app.use(express.json());

// Middleware that allows to read data from forms
app.use(express.urlencoded({extended: true}));


// Creating a new task
/*
	1. Add a function to check if there are duplicate tasks
		- If task is already exist, return error
		- If task doesn't exist, add to database
	2. The task data will be coming from the request body
	3. Create a Task object with a "name" field/property
*/

app.post('/tasks', (req, res) => {

	//Check if there are duplicate tasks
		//If there are no matches, the value of result is null
		Task.findOne({name: req.body.name}, (err, result) => {

			//if a document was found and the document's name matches the information sent via the client/Postman
			if (result != null && result.name == req.body.name){
				//Return a message to the client/postman
				return res.send("Duplicate task found");

			//if no document was found	
			} else {

				//Create a new Task and save it to the database
				let newTask = new Task({
					name: req.body.name
				});

				//save method will store the information to the database
				newTask.save((saveErr, savedTask) => {

					//if there are errors in saving
					if(saveErr){
						return console.error(saveErr)

					//no error found while creating the document	
					} else {

						//returns a status code of 201 and sends a message "New Task created."
						return res.status(201).send("New Task created.")

				}
			})
		}
	})
})


// Create a GET request to retrieve all documents
/*
	1. Retrieve all documents
	2. If error, print error
	3. Else, send success to client
*/

app.get('/tasks', (req, res) => {

	Task.find({}, (err, result) => {
		if (err) {
			console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})



app.get('/hello', (req, res) => {
	res.send('Hello World!')
})


//Create a User schema
const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	}
});

//Create a User model
const User = mongoose.model('User', userSchema);

//Create a POST route that will access the "/signup" route that will create a user
app.post('/signup', (req, res) => {
	const {username, password} = req.body;

			User.findOne({username: username}, (err, result) => {

			if (result != null && result.username == username){
				return res.send("Username already taken");

			} else {
				//Create a new user and save to database
				let newUser = new User({
					username: username,
					password: password
				});

				newUser.save((saveErr, savedUser) => {
					//Will send the error if it occured
					if(saveErr){
						return res.send(saveErr.message)

					} else {

						return res.status(201).send("New user registered.")

				}
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running on port ${port}`));

